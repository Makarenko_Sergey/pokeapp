package com.iridium.pokemons.features.list.repository.remote

import com.iridium.pokemons.features.list.repository.remote.response.PokemonsListResponse

interface PokemonsListRepository {

    suspend fun loadPokemons(limit: Int, offset: Int): PokemonsListResponse
}
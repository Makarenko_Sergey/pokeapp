package com.iridium.pokemons.features.details.usecases

import com.iridium.pokemons.base.UseCase
import com.iridium.pokemons.features.details.dao.PokemonDetails
import com.iridium.pokemons.features.details.dao.mapEntityToDao
import com.iridium.pokemons.features.details.dao.mapRemoteToDao
import com.iridium.pokemons.features.details.dao.mapRemoteToEntity
import com.iridium.pokemons.features.details.repository.local.LocalDetailsRepository
import com.iridium.pokemons.features.details.repository.remote.RemoteDetailsRepository

class LoadDetailsUseCase(
    private val remoteRepository: RemoteDetailsRepository,
    private val localRepository: LocalDetailsRepository
) : UseCase<String, PokemonDetails?> {

    override suspend fun execute(params: String): PokemonDetails? {
        val local = localRepository.loadPokemonByName(name = params)
        return if (local == null) {
            val remote = remoteRepository.loadDetails(name = params)
            val entity = mapRemoteToEntity(remote = remote)
            localRepository.insertPokemon(entity = entity)
            mapRemoteToDao(remote = remote)
        } else {
            mapEntityToDao(entity = local)
        }
    }

}
package com.iridium.pokemons.features.details.ui

import com.iridium.pokemons.features.details.dao.PokemonDetails
import io.uniflow.core.flow.data.UIState

data class DetailsUiContentState(val pokemonDetails: PokemonDetails?) : UIState()
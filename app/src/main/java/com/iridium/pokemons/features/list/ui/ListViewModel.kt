package com.iridium.pokemons.features.list.ui

import com.iridium.pokemons.base.BaseViewModel
import com.iridium.pokemons.features.list.usecases.LoadListUseCase
import com.iridium.pokemons.features.list.usecases.PageParam
import com.iridium.pokemons.utils.emptyString
import io.uniflow.core.flow.data.UIError
import io.uniflow.core.flow.data.UIState

class ListViewModel(
    private val loadListUseCase: LoadListUseCase,
) : BaseViewModel() {

    companion object {
        private const val PAGE_SIZE = 10
        private const val PREFETCH_COUNT = 10
    }

    fun pokemonWIthNAmeSelected(name: String) {
        action { sendEvent(OpenPokemonDetailsEvent(name = name)) }
    }

    fun load() {
        action(
            onAction = {
                setState(UIState.Loading)
                val data = loadListUseCase.execute(
                    PageParam(
                        PAGE_SIZE,
                        PREFETCH_COUNT
                    )
                )
                setState(
                    ListUiContentState(
                        data
                    )
                )
            },
            onError = { exception: Exception, _: UIState ->
                setState(
                    UIState.Failed(
                        emptyString(),
                        UIError(exception.localizedMessage, exception)
                    )
                )
            }
        )

    }
}
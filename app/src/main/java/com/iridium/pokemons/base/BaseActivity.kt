package com.iridium.pokemons.base

import androidx.appcompat.app.AppCompatActivity
import com.iridium.pokemons.R

open class BaseActivity : AppCompatActivity() {

    protected fun initToolbar() {
        setSupportActionBar(findViewById(R.id.toolbar))
    }

    protected fun showBackEnabled(showBack: Boolean) {
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(showBack)
            setDisplayShowHomeEnabled(showBack)
        }
    }

    protected fun setTitle(title: String) {

        supportActionBar?.title = title.capitalize()
    }

}
package com.iridium.pokemons.features.list.ui

import androidx.paging.PagingData
import com.iridium.pokemons.features.list.dao.PokemonShortDescr
import io.uniflow.core.flow.data.UIEvent
import io.uniflow.core.flow.data.UIState
import kotlinx.coroutines.flow.Flow

data class ListUiContentState(val data: Flow<PagingData<PokemonShortDescr>>) : UIState()

data class OpenPokemonDetailsEvent(val name: String) : UIEvent()
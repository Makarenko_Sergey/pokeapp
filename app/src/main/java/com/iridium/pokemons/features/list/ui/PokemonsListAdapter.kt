package com.iridium.pokemons.features.list.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.iridium.pokemons.R
import com.iridium.pokemons.features.list.dao.PokemonShortDescr
import com.iridium.pokemons.utils.loadImageFrom

class PokemonsListAdapter(
    private val onPokemonSelected: ((name: String?) -> Unit)?
) : PagingDataAdapter<PokemonShortDescr, PokemonListViewHolder>(PokemonListItemComparator) {

    override fun onBindViewHolder(holder: PokemonListViewHolder, position: Int) {
        val item = getItem(position)
        item ?: return
        holder.onBind(item = item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonListViewHolder {
        return PokemonListViewHolder(
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.pokemon_list_item, parent, false)
        ) {
            onPokemonSelected?.invoke(getItem(it)?.name)
        }
    }
}

class PokemonListViewHolder(view: View, onClick: (Int) -> Unit) : RecyclerView.ViewHolder(view) {

    private val name = view.findViewById<TextView>(R.id.pokemon_list_item_name)
    private val image = view.findViewById<ImageView>(R.id.pokemon_list_image)

    init {
        view.findViewById<View>(R.id.pokemon_list_item_root)
            .setOnClickListener { onClick(absoluteAdapterPosition) }
    }

    fun onBind(item: PokemonShortDescr) {
        name.text = item.name
        image.loadImageFrom(item.url)
    }
}

private object PokemonListItemComparator : DiffUtil.ItemCallback<PokemonShortDescr>() {
    override fun areItemsTheSame(oldItem: PokemonShortDescr, newItem: PokemonShortDescr) =
        oldItem.name == newItem.name

    override fun areContentsTheSame(
        oldItem: PokemonShortDescr,
        newItem: PokemonShortDescr
    ) = oldItem == newItem

}
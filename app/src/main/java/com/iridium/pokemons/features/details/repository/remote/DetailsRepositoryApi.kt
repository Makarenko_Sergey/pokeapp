package com.iridium.pokemons.features.details.repository.remote

import retrofit2.http.GET
import retrofit2.http.Path

interface DetailsRepositoryApi {

    @GET("api/v2/pokemon/{name}/")
    suspend fun loadDetails(@Path("name") name: String): DetailsResponse
}
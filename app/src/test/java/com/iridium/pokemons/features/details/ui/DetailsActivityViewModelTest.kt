package com.iridium.pokemons.features.details.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.iridium.pokemons.features.details.dao.PokemonDetails
import com.iridium.pokemons.features.details.usecases.LoadDetailsUseCase
import com.iridium.pokemons.utils.emptyString
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.uniflow.android.test.TestViewObserver
import io.uniflow.android.test.createTestObserver
import io.uniflow.core.flow.data.UIError
import io.uniflow.core.flow.data.UIState
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DetailsActivityViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var mockView: TestViewObserver

    private lateinit var viewModel: DetailsActivityViewModel

    @MockK
    private lateinit var useCase: LoadDetailsUseCase

    private val pokemonNameStub = "pokemon_name"

    private val pokemonStub = PokemonDetails(
        name = pokemonNameStub,
        height = 23,
        weight = 12,
        types = listOf(),
        sprites = listOf(),
        stats = listOf()
    )

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        viewModel = DetailsActivityViewModel(useCase)
        mockView = viewModel.createTestObserver()
    }


    @Test
    fun test_load_positive() {
        coEvery {
            useCase.execute(pokemonNameStub)
        } returns pokemonStub
        viewModel.loadPokemonByName(pokemonNameStub)

        mockView.verifySequence(
            UIState.Empty,
            UIState.Loading,
            DetailsUiContentState(pokemonStub)
        )
    }

    @Test
    fun test_load_negative() {
        coEvery {
            useCase.execute(pokemonNameStub)
        } throws IllegalArgumentException()
        viewModel.loadPokemonByName(pokemonNameStub)

        mockView.verifySequence(
            UIState.Empty,
            UIState.Loading,
            UIState.Failed(emptyString(), UIError())
        )
    }

}
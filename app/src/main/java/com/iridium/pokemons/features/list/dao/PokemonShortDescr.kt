package com.iridium.pokemons.features.list.dao

data class PokemonShortDescr(val name: String, val url: String)
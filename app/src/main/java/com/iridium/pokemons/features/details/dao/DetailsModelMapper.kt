package com.iridium.pokemons.features.details.dao

import com.iridium.pokemons.features.details.repository.remote.DetailsResponse
import com.iridium.pokemons.localstorage.PokemonDetailsEntity

fun mapRemoteToEntity(remote: DetailsResponse) =
    PokemonDetailsEntity(
        name = remote.name,
        weight = remote.weight,
        height = remote.height,
        type = remote.type.map { it.type.name },
        stats = remote.stats.map { it.stat.statName },
        sprites = remote.sprites.values.mapNotNull { it as? String }
    )

fun mapEntityToDao(entity: PokemonDetailsEntity) =
    PokemonDetails(
        name = entity.name,
        weight = entity.weight,
        height = entity.height,
        types = entity.type,
        stats = entity.stats,
        sprites = entity.sprites
    )

fun mapRemoteToDao(remote: DetailsResponse) =
    PokemonDetails(
        name = remote.name,
        weight = remote.weight,
        height = remote.height,
        types = remote.type.map { it.type.name },
        stats = remote.stats.map { it.stat.statName },
        sprites = remote.sprites.values.mapNotNull { it as? String }
    )
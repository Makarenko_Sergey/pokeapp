package com.iridium.pokemons.features.details.repository.remote

import com.iridium.pokemons.network.RetrofitNetworkProvider

class RemoteDetailsRepositoryImpl(networkProvider: RetrofitNetworkProvider) :
    RemoteDetailsRepository {

    private val api = networkProvider.buildApi(DetailsRepositoryApi::class.java)

    override suspend fun loadDetails(name: String): DetailsResponse {
        return api.loadDetails(name = name)
    }
}
package com.iridium.pokemons.features.details.dao

data class PokemonDetails(
    val name: String,
    val height: Int,
    val weight: Int,
    val types: List<String>,
    val sprites: List<String>,
    val stats: List<String>
)
package com.iridium.pokemons.features.list.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.iridium.pokemons.features.list.dao.mapNetworkToStorageEntity
import com.iridium.pokemons.features.list.repository.local.PokemonsListLocalRepository
import com.iridium.pokemons.features.list.repository.remote.PokemonsListRepository
import com.iridium.pokemons.localstorage.PokemonShortDescrEntity
import com.iridium.pokemons.localstorage.RemoteKeyEntity

@ExperimentalPagingApi
class ListRemoteMediator(
    private val remoteRepository: PokemonsListRepository,
    private val localRepository: PokemonsListLocalRepository
) : RemoteMediator<Int, PokemonShortDescrEntity>() {
    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, PokemonShortDescrEntity>
    ): MediatorResult {
        return try {
            val loadKey = when (loadType) {
                LoadType.REFRESH -> null
                LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> {
                    state.lastItemOrNull()
                        ?: return MediatorResult.Success(endOfPaginationReached = true)
                    localRepository.loadRemoteKeys()
                }
            }

            val limit = state.config.pageSize
            val currentOffset = loadKey?.firstOrNull()?.offset ?: 0
            val response = remoteRepository.loadPokemons(
                limit = limit,
                offset = currentOffset
            )

            localRepository.insertAll(response.pokemonsList.map { mapNetworkToStorageEntity(it) })
            localRepository.saveRemoteKeys(RemoteKeyEntity(offset = currentOffset + limit))

            MediatorResult.Success(
                endOfPaginationReached = response.nextUrl == null
            )
        } catch (e: Exception) {
            MediatorResult.Error(e)
        }
    }
}
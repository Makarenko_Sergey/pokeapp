package com.iridium.pokemons.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.iridium.pokemons.R

fun ImageView.loadImageFrom(imageUrl: String?) {
    imageUrl ?: return
    Glide
        .with(this)
        .load(imageUrl)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .placeholder(R.drawable.pokemon_placeholder)
        .into(this)
}
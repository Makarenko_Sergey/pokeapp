package com.iridium.pokemons.features.list.repository.remote

import com.iridium.pokemons.features.list.repository.remote.response.PokemonShortRemoteItem
import com.iridium.pokemons.features.list.repository.remote.response.PokemonsListResponse
import com.iridium.pokemons.network.RetrofitNetworkProvider

class PokemonListRemoteImpl(networkProvider: RetrofitNetworkProvider) : PokemonsListRepository {

    private val api = networkProvider.buildApi(apiClass = PokemonListRepositoryApi::class.java)

    override suspend fun loadPokemons(limit: Int, offset: Int): PokemonsListResponse {
        return api.loadPage(limit = limit, offset = offset)
    }

}
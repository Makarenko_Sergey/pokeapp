package com.iridium.pokemons.features.details.ui

import com.iridium.pokemons.base.BaseViewModel
import com.iridium.pokemons.features.details.usecases.LoadDetailsUseCase
import com.iridium.pokemons.utils.emptyString
import io.uniflow.core.flow.data.UIError
import io.uniflow.core.flow.data.UIState

class DetailsActivityViewModel(
    private val loadDetailsUseCase: LoadDetailsUseCase
) : BaseViewModel() {

    fun loadPokemonByName(name: String) {
        action(onAction = {
            setState(UIState.Loading)
            setState(DetailsUiContentState(loadDetailsUseCase.execute(params = name)))
        },
            onError = { exception: Exception, _: UIState ->
                setState(
                    UIState.Failed(
                        emptyString(),
                        UIError(exception.localizedMessage, exception)
                    )
                )
            })
    }
}

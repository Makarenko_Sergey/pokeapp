package com.iridium.pokemons.features.details.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.iridium.pokemons.R

class TextListAdapter : RecyclerView.Adapter<TextViewHolder>() {

    private val content = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextViewHolder {
        return TextViewHolder(
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.text_horizontal_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: TextViewHolder, position: Int) {
        holder.onBind(text = content[position])
    }

    override fun getItemCount() = content.size

    fun updateContent(newContent: List<String>) {
        content.clear()
        content.addAll(newContent)
        notifyDataSetChanged()
    }

}

class TextViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val textView = view.findViewById<TextView>(R.id.text_list_item)

    fun onBind(text: String) {
        textView.text = text
    }
}
package com.iridium.pokemons.features.list.repository.local

import androidx.paging.PagingSource
import com.iridium.pokemons.localstorage.PokemonShortDescrEntity
import com.iridium.pokemons.localstorage.PokemonsDao
import com.iridium.pokemons.localstorage.RemoteKeyEntity

class PokemonsListLocalRepositoryImpl(private val pokemonsDao: PokemonsDao) :
    PokemonsListLocalRepository {

    override suspend fun insertAll(pokemons: List<PokemonShortDescrEntity>) {
        pokemonsDao.insertAll(pokemons)
    }

    override fun loadPokemonsList(): PagingSource<Int, PokemonShortDescrEntity> {
        return pokemonsDao.getPokemonsList()
    }

    override suspend fun loadRemoteKeys(): List<RemoteKeyEntity> {
        return pokemonsDao.getRemoteKeys()
    }

    override suspend fun saveRemoteKeys(entity: RemoteKeyEntity) {
        pokemonsDao.saveRemoteKeys(entity)
    }
}
package com.iridium.pokemons.localstorage

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PokemonsDao {

    @Query("SELECT * from pokemons_short_descr")
    fun getPokemonsList(): PagingSource<Int, PokemonShortDescrEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list: List<PokemonShortDescrEntity>)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveRemoteKeys(redditKey: RemoteKeyEntity)

    @Query("SELECT * FROM remote_keys ORDER BY `offset` DESC")
    suspend fun getRemoteKeys(): List<RemoteKeyEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity: PokemonDetailsEntity)

    @Query("SELECT * FROM details WHERE name=:name LIMIT 1")
    suspend fun getPokemonDetailsByName(name: String): PokemonDetailsEntity?
}
package com.iridium.pokemons.features.details.repository.local

import com.iridium.pokemons.localstorage.PokemonDetailsEntity
import com.iridium.pokemons.localstorage.PokemonsDao

class LocalDetailsRepositoryImpl(
    private val dao: PokemonsDao
) : LocalDetailsRepository {
    override suspend fun loadPokemonByName(name: String): PokemonDetailsEntity? {
        return dao.getPokemonDetailsByName(name = name)
    }

    override suspend fun insertPokemon(entity: PokemonDetailsEntity) {
        dao.insert(entity = entity)
    }

}
package com.iridium.pokemons.base

interface UseCase<P, R> {
    suspend fun execute(params: P): R
}
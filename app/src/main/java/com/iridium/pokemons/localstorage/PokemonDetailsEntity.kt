package com.iridium.pokemons.localstorage

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "details")
data class PokemonDetailsEntity(
    @PrimaryKey val name: String,
    val height: Int,
    val weight: Int,
    val type: List<String>,
    val sprites: List<String>,
    val stats: List<String>
)
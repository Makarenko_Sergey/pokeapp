package com.iridium.pokemons.network

import com.iridium.pokemons.BuildConfig

class PokemonUrlProvider {

    fun getPokemonBaseUrl() = BuildConfig.POKEMON_URL
}
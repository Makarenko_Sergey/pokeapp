package com.iridium.pokemons.features.list.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.*
import com.iridium.pokemons.features.list.repository.local.PokemonsListLocalRepository
import com.iridium.pokemons.features.list.repository.remote.PokemonsListRepository
import com.iridium.pokemons.features.list.repository.remote.response.PokemonShortRemoteItem
import com.iridium.pokemons.features.list.repository.remote.response.PokemonsListResponse
import com.iridium.pokemons.localstorage.PokemonShortDescrEntity
import com.iridium.pokemons.localstorage.RemoteKeyEntity
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalPagingApi
class ListRemoteMediatorTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var remoteMediator: ListRemoteMediator

    @MockK
    private lateinit var remoteRepository: PokemonsListRepository

    @MockK
    private lateinit var localRepository: PokemonsListLocalRepository

    private val LIMIT = 20

    private val OFFSET = 10

    private var pagingState: PagingState<Int, PokemonShortDescrEntity> = PagingState(
        pages = listOf(
            PagingSource.LoadResult.Page(
                listOf(PokemonShortDescrEntity("", "")),
                null,
                null
            )
        ),
        null,
        PagingConfig(LIMIT),
        LIMIT
    )

    private val pokemonsList = listOf<PokemonShortRemoteItem>()
    private val pokemonsListResponseStub = PokemonsListResponse(100, "url", "prevUrl", pokemonsList)

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        remoteMediator = ListRemoteMediator(remoteRepository, localRepository)
    }

    @Test
    fun testLoadPage() {
        coEvery {
            remoteRepository.loadPokemons(LIMIT, OFFSET)

        } returns pokemonsListResponseStub
        coEvery {
            localRepository.loadRemoteKeys()

        } returns listOf(RemoteKeyEntity(OFFSET))
        coEvery { localRepository.insertAll(any()) }
        coEvery { localRepository.saveRemoteKeys(any()) }


        runBlocking {
            remoteMediator.load(LoadType.APPEND, pagingState)
            coVerifyAll {
                localRepository.loadRemoteKeys()
                remoteRepository.loadPokemons(LIMIT, OFFSET)
                localRepository.insertAll(any())
            }

            confirmVerified(localRepository)
            confirmVerified(remoteRepository)
        }

    }

}
package com.iridium.pokemons.features.details.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.iridium.pokemons.R
import com.iridium.pokemons.base.BaseActivity
import com.iridium.pokemons.utils.SpaceListItemDecorator
import com.iridium.pokemons.utils.gone
import com.iridium.pokemons.utils.visible
import io.uniflow.androidx.flow.onStates
import io.uniflow.core.flow.data.UIState
import org.koin.android.viewmodel.ext.android.viewModel

class DetailsActivity : BaseActivity() {

    companion object {
        private const val POKEMON_NAME_KEY = "POKEMON_NAME_KEY"

        @JvmStatic
        fun newIntent(context: Context, pokemonName: String) =
            Intent(context, DetailsActivity::class.java)
                .putExtra(POKEMON_NAME_KEY, pokemonName)
    }

    private val viewModel: DetailsActivityViewModel by viewModel()

    private val imagesAdapter by lazy { ImagesPagerAdapter() }
    private val typesAdapter by lazy { TextListAdapter() }
    private val statsAdapter by lazy { TextListAdapter() }
    private lateinit var nameTextView: TextView
    private lateinit var weightTextView: TextView
    private lateinit var heightTextView: TextView
    private lateinit var progress: View
    private lateinit var content: View
    private lateinit var imagesCounter: TextView
    private val imagesLayoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.details_activity)
        initToolbar()
        showBackEnabled(showBack = true)
        imagesCounter = findViewById(R.id.details_images_counter)
        nameTextView = findViewById(R.id.pokemon_name)
        weightTextView = findViewById(R.id.pokemon_weight)
        heightTextView = findViewById(R.id.pokemon_height)
        progress = findViewById(R.id.pokemon_details_progressBar)
        content = findViewById(R.id.pokemon_details_content)
        val imagesRv = findViewById<RecyclerView>(R.id.pokemon_images_rv)

        imagesRv.layoutManager = imagesLayoutManager
        imagesRv.adapter = imagesAdapter
        LinearSnapHelper().attachToRecyclerView(imagesRv)
        imagesRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                updateImagesCounter()
            }
        })

        val statsRv = findViewById<RecyclerView>(R.id.pokemon_stats_rv)
        val typesRv = findViewById<RecyclerView>(R.id.pokemon_types_rv)
        statsRv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.pokemon_list_item_distance)
        statsRv.addItemDecoration(SpaceListItemDecorator(spacingInPixels, false))
        statsRv.adapter = statsAdapter
        typesRv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        typesRv.addItemDecoration(SpaceListItemDecorator(spacingInPixels, false))
        typesRv.adapter = typesAdapter

        val name = intent?.getStringExtra(POKEMON_NAME_KEY)
        if (name.isNullOrEmpty()) {
            throw IllegalArgumentException("Pokemon name should be passed")
        } else {
            setTitle(name)
            viewModel.loadPokemonByName(name = name)
        }

        onStates(viewModel) { state: UIState ->
            when (state) {
                is UIState.Loading -> {
                    showLoadingState()
                }
                is UIState.Failed -> {
                    showFailedState()
                }
                is DetailsUiContentState -> {
                    showContentState(state)
                }
            }
        }
    }

    private fun updateImagesCounter() {
        val findFirstVisibleItemPosition = imagesLayoutManager.findFirstVisibleItemPosition()
        val currentPosition = if (findFirstVisibleItemPosition != RecyclerView.NO_POSITION) {
            findFirstVisibleItemPosition
        } else {
            0
        }
        imagesCounter.text = "${currentPosition + 1}/${imagesAdapter.itemCount}"
    }

    private fun showContentState(contentState: DetailsUiContentState) {
        progress.gone()
        content.visible()
        val pokemonDetails = contentState.pokemonDetails
        pokemonDetails ?: return
        nameTextView.text = pokemonDetails.name.capitalize()
        weightTextView.text = "${(pokemonDetails.weight * 0.1).toInt()} kg"
        heightTextView.text = "${(pokemonDetails.height * 10).toInt()} cm"
        imagesAdapter.updateValues(newValues = pokemonDetails.sprites)
        statsAdapter.updateContent(pokemonDetails.stats)
        typesAdapter.updateContent(pokemonDetails.types)

        imagesCounter.visible()
        updateImagesCounter()
    }

    private fun showFailedState() {
        progress.gone()
        Toast.makeText(this, getString(R.string.error_message), Toast.LENGTH_SHORT).show()
        finish()
    }

    private fun showLoadingState() {
        progress.visible()
        content.gone()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}


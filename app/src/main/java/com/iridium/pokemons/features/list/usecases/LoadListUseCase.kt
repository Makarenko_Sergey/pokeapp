package com.iridium.pokemons.features.list.usecases

import androidx.paging.*
import com.iridium.pokemons.base.UseCase
import com.iridium.pokemons.features.list.dao.PokemonShortDescr
import com.iridium.pokemons.features.list.dao.mapStorageEntityToDao
import com.iridium.pokemons.features.list.repository.ListRemoteMediator
import com.iridium.pokemons.features.list.repository.local.PokemonsListLocalRepository
import com.iridium.pokemons.features.list.repository.remote.PokemonsListRepository
import com.iridium.pokemons.localstorage.PokemonShortDescrEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LoadListUseCase(
    private val remoteRepo: PokemonsListRepository,
    private val localRepo: PokemonsListLocalRepository
) : UseCase<PageParam, Flow<PagingData<PokemonShortDescr>>> {

    @OptIn(ExperimentalPagingApi::class)
    override suspend fun execute(params: PageParam): Flow<PagingData<PokemonShortDescr>> {

        return Pager(
            config = PagingConfig(
                pageSize = params.limit,
                prefetchDistance = params.prefetchDistance),
            remoteMediator = ListRemoteMediator(
                remoteRepository = remoteRepo,
                localRepository = localRepo
            )
        ) { localRepo.loadPokemonsList() }
            .flow
            .map { pagindData: PagingData<PokemonShortDescrEntity> ->
                pagindData.map { mapStorageEntityToDao(it) }
            }

    }

}
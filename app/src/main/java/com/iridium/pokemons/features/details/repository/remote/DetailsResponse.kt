package com.iridium.pokemons.features.details.repository.remote

import com.squareup.moshi.Json


data class DetailsResponse(
    @field:Json(name = "name")
    val name: String,
    @field:Json(name = "height")
    val height: Int,
    @field:Json(name = "weight")
    val weight: Int,
    @field:Json(name = "types") val type: List<PokemonType>,
    @field:Json(name = "sprites") val sprites: Map<String, Object>,
    @field:Json(name = "stats") val stats: List<Stats>
)

data class PokemonType(
    @field:Json(name = "type") val type: PokemonTypeName
)

data class PokemonTypeName(
    @field:Json(name = "name") val name: String
)

data class Stats(
    @field:Json(name = "stat") val stat: Stat
)

data class Stat(
    @field:Json(name = "name") val statName: String
)
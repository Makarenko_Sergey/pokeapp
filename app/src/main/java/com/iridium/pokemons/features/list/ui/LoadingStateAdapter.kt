package com.iridium.pokemons.features.list.ui

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.ContentLoadingProgressBar
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.iridium.pokemons.R
import com.iridium.pokemons.utils.gone
import com.iridium.pokemons.utils.visible

class LoadingStateAdapter : LoadStateAdapter<LoadingStateViewHolder>() {

    override fun onBindViewHolder(holder: LoadingStateViewHolder, loadState: LoadState) {
        holder.onBind(loadState = loadState)
        Log.v("LOADING1", "onbindloading")
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): LoadingStateViewHolder {
        return LoadingStateViewHolder(
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_load_state_view, parent, false)
        )
    }


}

class LoadingStateViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val progress =
        view.findViewById<ContentLoadingProgressBar>(R.id.load_list_state_progress)

    fun onBind(loadState: LoadState) {
        if (loadState is LoadState.Loading) {
            itemView.visible()
        } else {
            itemView.gone()
        }
    }
}
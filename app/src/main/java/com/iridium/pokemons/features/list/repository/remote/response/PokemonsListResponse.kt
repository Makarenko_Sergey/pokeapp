package com.iridium.pokemons.features.list.repository.remote.response

import com.squareup.moshi.Json

data class PokemonsListResponse(
    @field:Json(name = "count")
    val count: Int,
    @field:Json(name = "next")
    val nextUrl: String?,
    @field:Json(name = "previous")
    val prevUrl: String?,
    @field:Json(name = "results")
    val pokemonsList: List<PokemonShortRemoteItem>
)
package com.iridium.pokemons.features.list.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.PagingData
import com.iridium.pokemons.features.list.dao.PokemonShortDescr
import com.iridium.pokemons.features.list.usecases.LoadListUseCase
import com.iridium.pokemons.features.list.usecases.PageParam
import com.iridium.pokemons.utils.emptyString
import com.nhaarman.mockitokotlin2.any
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.uniflow.android.test.TestViewObserver
import io.uniflow.android.test.createTestObserver
import io.uniflow.core.flow.data.UIError
import io.uniflow.core.flow.data.UIState
import kotlinx.coroutines.flow.flow
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ListViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var loadListUseCase: LoadListUseCase

    private lateinit var viewModel: ListViewModel
    private lateinit var mockView: TestViewObserver

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        viewModel = ListViewModel(loadListUseCase = loadListUseCase)
        mockView = viewModel.createTestObserver()
    }

    @Test
    fun test_load_negative() {

        val exception = IllegalArgumentException()
        coEvery {
            loadListUseCase.execute(
                PageParam(
                    10,
                    10
                )
            )
        } throws exception
        viewModel.load()
        mockView.verifySequence(
            UIState.Empty,
            UIState.Loading,
            UIState.Failed(emptyString(), UIError())
        )
    }

    @Test
    fun test_load_positive() {

        val flow = flow { emit(any<PagingData<PokemonShortDescr>>()) }

        coEvery {
            loadListUseCase.execute(
                PageParam(
                    10,
                    10
                )
            )
        } returns flow
        viewModel.load()

        mockView.verifySequence(
            UIState.Empty,
            UIState.Loading,
            ListUiContentState(data = flow)
        )
    }
}
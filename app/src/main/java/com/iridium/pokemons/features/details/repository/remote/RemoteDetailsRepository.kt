package com.iridium.pokemons.features.details.repository.remote

interface RemoteDetailsRepository {

    suspend fun loadDetails(name: String): DetailsResponse
}
package com.iridium.pokemons.features.list.repository.local

import androidx.paging.PagingSource
import com.iridium.pokemons.localstorage.PokemonShortDescrEntity
import com.iridium.pokemons.localstorage.RemoteKeyEntity

interface PokemonsListLocalRepository {

    suspend fun insertAll(pokemons: List<PokemonShortDescrEntity>)

    fun loadPokemonsList(): PagingSource<Int, PokemonShortDescrEntity>

    suspend fun loadRemoteKeys(): List<RemoteKeyEntity>

    suspend fun saveRemoteKeys(entity: RemoteKeyEntity)

}
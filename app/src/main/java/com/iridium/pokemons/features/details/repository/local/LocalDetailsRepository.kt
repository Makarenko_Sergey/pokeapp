package com.iridium.pokemons.features.details.repository.local

import com.iridium.pokemons.localstorage.PokemonDetailsEntity

interface LocalDetailsRepository {

    suspend fun loadPokemonByName(name: String): PokemonDetailsEntity?
    suspend fun insertPokemon(entity: PokemonDetailsEntity)
}
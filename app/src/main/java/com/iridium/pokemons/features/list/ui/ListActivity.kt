package com.iridium.pokemons.features.list.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.iridium.pokemons.R
import com.iridium.pokemons.base.BaseActivity
import com.iridium.pokemons.features.details.ui.DetailsActivity
import com.iridium.pokemons.utils.SpaceListItemDecorator
import com.iridium.pokemons.utils.gone
import com.iridium.pokemons.utils.visible
import io.uniflow.androidx.flow.onStates
import io.uniflow.androidx.flow.onTakeEvents
import io.uniflow.core.flow.data.UIState
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel


class ListActivity : BaseActivity() {

    private val viewModel: ListViewModel by viewModel()

    private lateinit var pokemonRecyclerView: RecyclerView
    private lateinit var emptyView: View
    private lateinit var progress: View
    private val adapter = PokemonsListAdapter() { pokemonName: String? ->
        pokemonName?.let {
            viewModel.pokemonWIthNAmeSelected(name = it)
        }
    }
    private val concatAdapter = adapter.withLoadStateFooter(footer = LoadingStateAdapter())

    private val COLUMNS_AMOUNT = 3

    private fun openDetailsScreen(pokemonName: String) {
        startActivity(DetailsActivity.newIntent(context = this, pokemonName = pokemonName))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_activity)
        initToolbar()
        setTitle(getString(R.string.pokemons))
        showBackEnabled(showBack = false)
        pokemonRecyclerView = findViewById(R.id.pokemon_list_rv)
        emptyView = findViewById(R.id.pokemon_list_empty)
        progress = findViewById(R.id.pokemon_list_progressBar)
        pokemonRecyclerView.layoutManager =
            GridLayoutManager(this, COLUMNS_AMOUNT)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.pokemon_list_item_distance)
        pokemonRecyclerView.addItemDecoration(SpaceListItemDecorator(spacingInPixels))
        pokemonRecyclerView.setHasFixedSize(true)
        pokemonRecyclerView.adapter = concatAdapter
        viewModel.load()

        onTakeEvents(viewModel) {
            if (it is OpenPokemonDetailsEvent) {
                openDetailsScreen(pokemonName = it.name)
            }
        }

        onStates(viewModel) { state: UIState ->
            when (state) {
                is UIState.Loading -> {
                    showProgressState()
                }
                is UIState.Failed -> {
                    showFailedState()
                }
                is UIState.Empty -> {
                    showEmptyState()
                }
                is ListUiContentState -> {
                    showContentState(state)
                }
            }
        }
    }

    private fun showFailedState() {
        progress.gone()
        Toast.makeText(this, getString(R.string.error_message), Toast.LENGTH_SHORT).show()
        finish()
    }

    private fun showContentState(
        contentState: ListUiContentState
    ) {
        progress.gone()
        emptyView.gone()
        pokemonRecyclerView.visible()
        lifecycleScope.launch {
            contentState.data.collectLatest {
                adapter.submitData(pagingData = it)
            }
        }
    }

    private fun showProgressState() {
        progress.visible()
        emptyView.gone()
        pokemonRecyclerView.gone()
    }

    private fun showEmptyState() {
        progress.gone()
        emptyView.visible()
        pokemonRecyclerView.gone()
    }
}
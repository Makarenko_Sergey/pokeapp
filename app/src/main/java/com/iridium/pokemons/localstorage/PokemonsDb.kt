package com.iridium.pokemons.localstorage

import android.content.Context
import androidx.room.*
import com.iridium.pokemons.utils.emptyString

@Database(
    entities = [PokemonShortDescrEntity::class, RemoteKeyEntity::class, PokemonDetailsEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class PokemonsDb : RoomDatabase() {

    abstract fun pokemonsDao(): PokemonsDao

    companion object {

        private const val DB_NAME = "pokemons_db"

        @Volatile
        private var INSTANCE: PokemonsDb? = null

        fun getDb(context: Context): PokemonsDb {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PokemonsDb::class.java,
                    DB_NAME
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}

object Converters {
    @TypeConverter
    fun fromString(value: String?): List<String> {
        value ?: return emptyList()
        return value
            .replace("[", emptyString())
            .replace("]", emptyString())
            .split(",")
    }

    @TypeConverter
    fun fromArrayList(list: List<String>): String {
        return list.joinToString(separator = ",")
    }
}
package com.iridium.pokemons.features.list.repository.remote

import com.iridium.pokemons.features.list.repository.remote.response.PokemonsListResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface PokemonListRepositoryApi {

    @GET("api/v2/pokemon/")
    suspend fun loadPage(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): PokemonsListResponse
}
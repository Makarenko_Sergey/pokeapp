package com.iridium.pokemons.features.details.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.iridium.pokemons.R
import com.iridium.pokemons.utils.loadImageFrom

class ImagesPagerAdapter(
) : RecyclerView.Adapter<ImagesViewHolder>() {

    private val images = mutableListOf<String>()

    fun updateValues(newValues: List<String>) {
        images.clear()
        images.addAll(newValues)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImagesViewHolder {
        return ImagesViewHolder(
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.image_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ImagesViewHolder, position: Int) {
        holder.onBind(imageUrl = images[position])
    }

    override fun getItemCount(): Int {
        return images.size
    }
}

class ImagesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val image = view.findViewById<ImageView>(R.id.pokemon_image)
    fun onBind(imageUrl: String) {
        image.loadImageFrom(imageUrl)
    }
}
package com.iridium.pokemons.localstorage

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemons_short_descr")
data class PokemonShortDescrEntity(
    @PrimaryKey val name: String,
    val url: String
)
package com.iridium.pokemons.features.list.dao

import android.net.Uri
import com.iridium.pokemons.BuildConfig
import com.iridium.pokemons.features.list.repository.remote.response.PokemonShortRemoteItem
import com.iridium.pokemons.localstorage.PokemonShortDescrEntity
import com.iridium.pokemons.utils.emptyString

fun mapNetworkToStorageEntity(networkModel: PokemonShortRemoteItem): PokemonShortDescrEntity {
    return with(networkModel) {
        PokemonShortDescrEntity(
            name = name,
            url = parsePokemonUrl(url = url)
        )
    }
}

fun mapStorageEntityToDao(entity: PokemonShortDescrEntity): PokemonShortDescr {
    return PokemonShortDescr(
        name = entity.name,
        url = entity.url
    )
}

const val ID_PLACEHOLDER = "{id}"

private fun parsePokemonUrl(url: String): String {
    return Uri.parse(url).lastPathSegment?.let {
        BuildConfig.POKEMON_IMAGE_API.replace(
            ID_PLACEHOLDER,
            it
        )
    } ?: emptyString()

}
package com.iridium.pokemons.base

import io.uniflow.androidx.flow.AndroidDataFlow
import io.uniflow.core.flow.data.UIState

open class BaseViewModel : AndroidDataFlow(defaultState = UIState.Empty) {

}
package com.iridium.pokemons.di

import android.app.Application
import com.iridium.pokemons.features.details.repository.local.LocalDetailsRepository
import com.iridium.pokemons.features.details.repository.local.LocalDetailsRepositoryImpl
import com.iridium.pokemons.features.details.repository.remote.RemoteDetailsRepository
import com.iridium.pokemons.features.details.repository.remote.RemoteDetailsRepositoryImpl
import com.iridium.pokemons.features.details.ui.DetailsActivityViewModel
import com.iridium.pokemons.features.details.usecases.LoadDetailsUseCase
import com.iridium.pokemons.features.list.repository.local.PokemonsListLocalRepository
import com.iridium.pokemons.features.list.repository.local.PokemonsListLocalRepositoryImpl
import com.iridium.pokemons.features.list.repository.remote.PokemonListRemoteImpl
import com.iridium.pokemons.features.list.repository.remote.PokemonsListRepository
import com.iridium.pokemons.features.list.ui.ListViewModel
import com.iridium.pokemons.features.list.usecases.LoadListUseCase
import com.iridium.pokemons.localstorage.PokemonsDb
import com.iridium.pokemons.network.PokemonUrlProvider
import com.iridium.pokemons.network.RetrofitNetworkProvider
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class DiWrapper(application: Application) {

    private val viewModulesModule = module {
        viewModel { ListViewModel(loadListUseCase = get()) }
        viewModel { DetailsActivityViewModel(loadDetailsUseCase = get()) }
    }

    private val dbModule = module {
        single { PokemonsDb.getDb(context = application).pokemonsDao() }
    }

    private val networkModule = module {
        single { RetrofitNetworkProvider(baseUrl = get()) }
        factory { PokemonUrlProvider().getPokemonBaseUrl() }
    }

    private val repositoryModule = module {
        single<PokemonsListRepository> { PokemonListRemoteImpl(networkProvider = get()) }
        single<PokemonsListLocalRepository> { PokemonsListLocalRepositoryImpl(pokemonsDao = get()) }
        single<RemoteDetailsRepository> { RemoteDetailsRepositoryImpl(networkProvider = get()) }
        single<LocalDetailsRepository> { LocalDetailsRepositoryImpl(dao = get()) }
    }
    private val useCasesModule = module {
        factory { LoadListUseCase(remoteRepo = get(), localRepo = get()) }
        factory { LoadDetailsUseCase(remoteRepository = get(), localRepository = get()) }
    }

    init {
        startKoin {
            androidContext(application)
            modules(
                listOf(
                    networkModule,
                    repositoryModule,
                    viewModulesModule,
                    useCasesModule,
                    dbModule
                )
            )
        }
    }
}
package com.iridium.pokemons.features.list.repository.remote.response

import com.squareup.moshi.Json

data class PokemonShortRemoteItem(
    @field:Json(name = "name")
    val name: String,
    @field:Json(name = "url")
    val url: String
)
package com.iridium.pokemons.application

import android.app.Application
import com.iridium.pokemons.di.DiWrapper

class PokeAppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initDi()
    }

    private fun initDi() {
        DiWrapper(this)
    }
}
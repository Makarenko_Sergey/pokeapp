package com.iridium.pokemons.localstorage

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "remote_keys")
data class RemoteKeyEntity(@PrimaryKey val offset: Int)
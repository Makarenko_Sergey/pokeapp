package com.iridium.pokemons.features.list.usecases

data class PageParam(val limit: Int, val prefetchDistance: Int)